

const express = require('express');
const mysql = require('mysql');
const app = express();
const cors = require('cors');
app.use(express.json());
app.use(cors());


//create connection to database 
const db = mysql.createConnection({
    user:'root',
    host:'localhost',
    password:'password',
    database:'famegalaxy'
});

//insert the va;ue into database


app.listen(4000,()=>{
    console.log('Server is running on port No: 4000');
})


//for retrive the data from the database
app.get("/employee", (req,res)=>{
    db.query("SELECT * FROM famegalaxyemployee", (err, result)=>{
        if(err){
            console.log(err);
        }else{
            res.send(result);
        }
    })
})


app.post("/create",(req,res)=>{
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const contact = req.body.contact;
    const qualification = req.body.qualification;
    const address = req.body.address;
    
db.query("INSERT INTO famegalaxyemployee (firstname,lastname,email,qualification,contact,address) VALUES (?,?,?,?,?,?)", 
[firstname,lastname,email,qualification,contact,address],
(error, result)=>{
    if(error){
        console.log(error);
    }else{
        res.send("Data Inserted Successfully.....");
    }
}
)
})