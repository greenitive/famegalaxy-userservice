import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import { Navbar } from './Component/Layout/Navbar'
import {Actors} from './Component/Pages/Actors';
import {Celebrity} from './Component/Pages/Celebrity';
import {Cricket} from './Component/Pages/Cricket';
import {Fame} from './Component/Pages/Fame';
import {Football} from './Component/Pages/Football';
import {MailBox} from './Component/Pages/MailBox';
import {Politician} from './Component/Pages/Politician';
import {Sports} from './Component/Pages/Sports';
import {Theater} from './Component/Pages/Theater';
import {AddUser} from './Component/users/AddUser';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
function App() {
  return (
    <div className="App">
      <Router>
      <Navbar />
        <Switch>
          <Route exact path='/' component={Fame}/>
          <Route exact path='/Actors' component={Actors}/>
          <Route exact path='/Celebrity' component={Celebrity}/>
          <Route exact path='/Cricket' component={Cricket}/>
          <Route exact path='/Football' component={Football}/>
          <Route exact path='/MailBox' component={MailBox}/>
          <Route exact path='/Politician' component={Politician}/>
          <Route exact path='/Sports' component={Sports}/>
          <Route exact path='/Theater' component={Theater}/>
          <Route exact path='/AddUser' component={AddUser}/>
        </Switch>
      </Router>
     
    </div>
  );
}

export default App;
