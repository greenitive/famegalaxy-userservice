import React from 'react';
import {Link, NavLink} from 'react-router-dom';
export const Navbar = () => {
    return (
      
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
  <div className="container">
  <NavLink className="navbar-brand" exact to="/">FameGalaxy</NavLink>
    <ul className="navbar-nav mr-auto">
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Actors">Actors </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Celebrity">Celebrity</NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Cricket">Cricket </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Football">Football </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/MailBox">MailBox </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Politician">Politician </NavLink>
      </li> 
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Sports">Sports </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link" exact to="/Theater">Theater </NavLink>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <button className="btn btn-outline-dark my-2 my-sm-0 navbar-dark bg-primary mr-4" type="submit">Login</button>
      <button className="btn btn-outline-dark my-2 my-sm-4 navbar-dark bg-primary " type="submit">SignUp</button>
    </form>
  </div>
  <Link className="btn btn-outline-light"to='/AddUser'>Add User</Link>
</nav>
     
    )
}
