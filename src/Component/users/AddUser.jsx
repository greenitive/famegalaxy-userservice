import React, { useState } from 'react'
import axios from 'axios';

export const AddUser = (props) => {
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("")
    const [email, setEmail] = useState("")
    const [qualification, setQualification] = useState("")
    const [contact, setContact] = useState("")
    const [address, setAddress] = useState("")



    const employeeRecords = () => {
        console.log("Submit Button Clicked!!!!");

        axios.post("http://localhost:4000/create", {
            firstname: firstname,
            lastname: lastname,
            contact: contact,
            email: email,
            address: address,
            qualification: qualification
        }).then(() => {
            console.log('Successfully Submitted!!!!');
            //this array distructuring is for when we click on submit button the data is display automatically
            //this is for when we want to click the button and display the data for
            //that we can destructuring the data its called array destructuring .
            //for display the data from the page then uncomment the below code
            setEmployeeList([...employeeList, {
                firstname: firstname,
                lastname: lastname,
                email: email,
                contact: contact,
                qualification: qualification,
                address: address
            }])
        })
    }
    //show records
    const [employeeList, setEmployeeList] = useState([]);

    const showEemployeeRecords = () => {
        console.log("Please wait employee record will be displayed shortly...");
        axios.get("http://localhost:4000/employee").then((response) => {
            setEmployeeList(response.data);
        });
    }

  //create delete function here

  const deleteEmployee =(contact)=>{
    axios.delete(`http://localhost:4000/delete/${contact}`);
  }
    return (
        <div className="container">

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Email</th>
                        <th scope="col">Contact</th>
                        <th scope="col">Qualification</th>
                        <th scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        employeeList.map((value, key) => {
                            return <tr>
                                <td>{value.firstname}</td>
                                <td>{value.lastname}</td>
                                <td>{value.email}</td>
                                <td>{value.contact}</td>
                                <td>{value.qualification}</td>
                                <td>{value.address}</td>
                                
                            </tr>

                        })
                    }

                </tbody>
            </table>
                    <br/>  <br/>  <br/>  <br/>  <br/> 
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">First-Name</label>
                <div className="col-sm-10">
                    <input type="text" onChange={(event) => { setFirstname(event.target.value) }} className="form-control" id="inputPassword" name="firstname" placeholder="First-Name" />
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Last-Name</label>
                <div className="col-sm-10">
                    <input type="text" onChange={(event) => { setLastname(event.target.value) }} className="form-control" id="inputPassword" name="lastname" placeholder="Last-Name" />
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Email</label>
                <div className="col-sm-10">
                    <input type="Email" onChange={(event) => { setEmail(event.target.value) }} className="form-control" id="inputPassword" name="email" placeholder="Email" />
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Contact Number</label>
                <div className="col-sm-10">
                    <input type="number" className="form-control" onChange={(event) => { setContact(event.target.value) }} id="inputPassword" name="contact" placeholder="Contact " />
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Qualification</label>
                <div className="col-sm-10">
                    <input type="text" onChange={(event) => { setQualification(event.target.value) }} className="form-control" id="inputPassword" name="qualification" placeholder="Qualification" />
                </div>
            </div>
            <div className="form-group row">
                <label className="col-sm-2 col-form-label">Address</label>
                <div className="col-sm-10">
                    <input type="text" onChange={(event) => { setAddress(event.target.value) }} className="form-control" id="inputPassword" name="address" placeholder="Address" />
                </div>
            </div>
            <button onClick={employeeRecords} className="btn btn-primary lm-60px mr-4">Submit</button>
            <button onClick={showEemployeeRecords} className="btn btn-primary lm-60px mr-2">Show Records</button>
        </div>
    )
}
